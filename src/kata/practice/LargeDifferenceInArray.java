package kata.practice;

public class LargeDifferenceInArray {
    int largeDiff(int arr[], int arr_size) {
        int large_diff = arr[1] - arr[0];
        System.out.println(large_diff);
        int min_element = arr[0];
        int i;
        for (i = 1; i < arr_size; i++) {
            if (arr[i] - min_element > large_diff)
                large_diff = arr[i] - min_element;
            if (arr[i] < min_element)
                min_element = arr[i];
        }
        return large_diff;
    }
}
