package kata.practice;

public class HumanReadableTime {
    public  String makeReadable(int seconds) {
        int HH = 0;
        int MM = 0;
        int SS = 0;
        int secondsRemaining;
        if (seconds <= 359999) {
            if (seconds < 60) {
                SS = seconds;
            } else {
                secondsRemaining = seconds / 60;
                if (secondsRemaining < 60) {
                    MM = secondsRemaining;
                    SS = seconds - (secondsRemaining * 60);
                } else {
                    SS = seconds - (secondsRemaining * 60);
                    MM = secondsRemaining % 60;
                    secondsRemaining = secondsRemaining / 60;
                    HH = secondsRemaining;
                }
            }
            String hoursString = String.valueOf(HH).length() == 2 ? String.valueOf(HH) : "0" + String.valueOf(HH);
            String minutesString = String.valueOf(MM).length() == 2 ? String.valueOf(MM) : "0" + String.valueOf(MM);
            String secondsString = String.valueOf(SS).length() == 2 ? String.valueOf(SS) : "0" + String.valueOf(SS);
            return hoursString + ":" + minutesString + ":" + secondsString;
        }
        return "Maximum time exceeded.";
    }
}
