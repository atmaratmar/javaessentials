package kata.practice;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class CheckPassword {
   public void isPasswordStrong(){

       // TODO Auto-generated method stub
       Scanner scan = new Scanner(System.in);
       System.out.println("Enter the password");
       String inputPassword = scan.next();

           // Checking lower alphabet in string
           int n = inputPassword.length();
           boolean hasLower = false, hasUpper = false,
                   hasDigit = false, specialChar = false;
           Set<Character> set = new HashSet<Character>(
                   Arrays.asList('!', '@', '#', '$', '%', '^', '&',
                           '*', '(', ')', '-', '+'));
           for (char i : inputPassword.toCharArray())
           {
               if (Character.isLowerCase(i))
                   hasLower = true;
               if (Character.isUpperCase(i))
                   hasUpper = true;
               if (Character.isDigit(i))
                   hasDigit = true;
               if (set.contains(i))
                   specialChar = true;
           }

           // Strength of password
           System.out.print("Strength of password:- ");
           if (hasDigit && hasLower && hasUpper && specialChar
                   && (n >= 8))
               System.out.print(" Strong");
           else if ((hasLower && hasUpper && hasDigit )
                   && (n >= 8))
               System.out.print(" Moderate");
           else if ((hasLower && hasUpper && specialChar )
                   && (n >= 8))
               System.out.print(" Moderate");
           else if ((hasLower && hasUpper )
                   && (n >= 8))
               System.out.print(" Moderate");
           else
               System.out.print(" Weak");


       // Driver Code
    }


}
