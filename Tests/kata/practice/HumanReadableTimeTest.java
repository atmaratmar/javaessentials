package kata.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanReadableTimeTest extends HumanReadableTime {

    @Test
    void testMakeReadable() {
        HumanReadableTime hm = new HumanReadableTime();
        String SS=  hm.makeReadable(359999);
        String time = "99:59:59";
        assertEquals(SS,time);
    }
}